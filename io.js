class Io{
	constructor(game){
		this.game = game;
		this.game.setIo(this);
		this.$Container = $('<div class="snakeContainer">');
		this.matrixDisplay = new Display(15, 15);
		this.$score = $('<input type="text" readonly="true">');

		this.setupKeylistner();
		this.fillContainer();
	}

	setupKeylistner(){
		$("body").keydown(event =>{
			const BUTTONMAP = {
				37: this.game.BTN_UP,
				38: this.game.BTN_LEFT,
				39: this.game.BTN_DOWN,
				40: this.game.BTN_RIGHT
			};

			const virtualButton = BUTTONMAP[event.which];
			if(virtualButton) this.game.handleButton(virtualButton);
		});
	}

	fillContainer(){
		this.$Container.append(this.matrixDisplay.$canvasElement);
		this.$Container.append(this.$score);
	}

	update(){
		this.matrixDisplay.draw(this.game.snake, this.game.map);
		this.$score.val(this.game.score);
	}
}


newGame = new SnakeGame();
newIo = new Io(newGame);
newIo.$Container.appendTo($('body'));
