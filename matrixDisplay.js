class Display{
	constructor(lengthPixels, widthPixels){
		this.pixelSize = 20;
		this.width = widthPixels * this.pixelSize;
		this.length = lengthPixels * this.pixelSize;
		this.$canvasElement = $('<canvas class="canvasSnake" width="'+ this.width +'" height="'+ this.length +'">Your Browser does not display canvas</canvas>');

		
	}
	draw(snake, map){
		let ctx = this.$canvasElement[0].getContext("2d");
		const mapPixels = map.getPixels();
		const snakePixels = snake.getPixels();
		const food = map.foodPosition;

		ctx.clearRect(0, 0, this.width, this.length);
		ctx.beginPath();
		ctx.fillStyle = "red";
		ctx.arc(food.x * 20 + 10,food.y * 20 + 10, 10, 0, Math.PI * 2);
		ctx.fill();
		ctx.stroke();
		ctx.closePath();

		for(let i = 0; i < snakePixels.length; i++){
			ctx.fillStyle = "green";
			ctx.fillRect(snakePixels[i][1] * 20, snakePixels[i][0] * 20, 20, 20);
			ctx.strokeRect(snakePixels[i][1] * 20, snakePixels[i][0] * 20, 20, 20);

		}
	}
}