class Map{
	constructor(xBlocks, yBlocks){
		this.size = {x: xBlocks, y: yBlocks};
		this.foodPosition = {};
		this.pixels = [];

		for(let i = 0; i < this.size.y; i++){
			this.pixels.push([]);
			while(this.pixels[i].length < this.size.x) this.pixels[i].push(0);
		}
	}
	getPixels(){
		return this.pixels;
	}

	placeNewFood(randPosition){
		this.foodPosition = randPosition;

	}
}