class CollisionError extends Error {}

class SnakeGame{
	constructor(){
		Object.assign(this, createConstants([
		"BTN_LEFT",
		"BTN_UP",
		"BTN_RIGHT",
		"BTN_DOWN"
		]));

		this.snake = new SnakeBlocks();
		this.map = new Map(15, 15);
		this.score = 0;
		this.gameOver = false;
		this.foodEaten = true;


		this.setMoveTimeout();
		this.setIo();
		this.checkFood();
	}
	setIo(io){
		this.io = io;
	}
	handleButton(virtualButton){
		switch(virtualButton){
			case this.BTN_LEFT:{
				this.snake.changeDirection({x: -1, y: 0});
				break;
			}
			case this.BTN_UP: {
				this.snake.changeDirection({x: 0 , y:-1});
				break;
			}
			case this.BTN_RIGHT:{
				this.snake.changeDirection({x: 1, y: 0});
				break;
			}
			case this.BTN_DOWN:{
				this.snake.changeDirection({x: 0, y: 1});
				break;
			}
		}
	}

	moveSnake(){
		this.snake.move(this.foodEaten);
		this.foodEaten = false;
	}

	setMoveTimeout(){
		this.clearMoveTimeout();
		const timeout = 100;

		this.moveTimeout = setTimeout(()=>{

			this.checkFood();
			this.moveSnake();
			this.checkGameover();
			this.io.update();
			if(!this.gameOver) this.setMoveTimeout();
		}, timeout);
	}

	clearMoveTimeout(){
		clearTimeout(this.moveTimeout);
	}

	Collision(snakePixels, mapPixels){
		const headCoords = snakePixels[0];

		if((mapPixels[headCoords[0]] || [])[headCoords[1]] === undefined){
			throw new CollisionError('Snake collision with a wall');
		}
		else{
			for(let i = 0; i < snakePixels.length; i++){
				for(let j = i + 1; j < snakePixels.length; j++){
					if (snakePixels[i][0] == snakePixels[j][0] && snakePixels[i][1] == snakePixels[j][1]){
						throw new CollisionError('Snake selfcollision');
					}
				}
			}
		}
		if(this.map.foodPosition.x === headCoords[1] && this.map.foodPosition.y === headCoords[0]){
			this.removeFood();
		}
	}

	checkCollision(){
		try{
			this.Collision(this.snake.getPixels(), this.map.getPixels());
			return false;
		}
		catch(e){
			if(e instanceof CollisionError) return true;
			else throw e;
		}
	}

	checkFood(){
		if(this.foodEaten) this.map.placeNewFood(this.randomFreeSpot());

	}

	randomFreeSpot(){
		const snakePixels = this.snake.getPixels();
		const mapSize = this.map.size;
		while(snakePixels.length < (mapSize.x * mapSize.y)){
			let x = Math.floor(Math.random() * mapSize.x),
				y = Math.floor(Math.random() * mapSize.y);

			for(let i = 0; i < snakePixels.length; i++){
				if(snakePixels[i][0] !== y && snakePixels[i][1] !== x) return {y, x};
			}
		}
		return {x:0, y:0};
	}

	removeFood(){
		this.map.pixels[this.snake.pixels[0][0]][this.snake.pixels[0][1]] = 0;
		this.score += 50;
		this.foodEaten = true;
	}

	checkGameover(){
		if(this.checkCollision()) {
			this.clearMoveTimeout();
			this.gameOver = true;
		}
	}
}
