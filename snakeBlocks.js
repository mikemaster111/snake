class SnakeBlocks{
	constructor(){
		this.pixels = [[5, 5]]; // y axis, x axis
		this.direction = {x: 0, y: 1};
	}
	getPixels(){
		return this.pixels;
	}

	move(foodEaten){
		this.pixels.unshift([this.pixels[0][0] + this.direction.x, this.pixels[0][1] + this.direction.y]);
		if(!foodEaten) this.pixels.pop();
	}

	changeDirection(newDirection){
		if(this.direction.x + newDirection.x === 0 || this.direction.y + newDirection.y === 0 ) return;
		this.direction = newDirection;
	}
}
